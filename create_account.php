<h2>Create Account</h2>

<form action="<?php echo $root_url;?>?page=create_account" Method="POST">
Username: <input type="text" name="username"> <br/>
Full Name: <input type="text" name="full_name"> <br/>
Password: <input type="text" name="password"> <br/>
Email: <input type="text" name="email"> <br/>
Gender: <select name="gender"><option value="M">Male</option><option value="F">Female</option></select><br/>
Birthday: <input type="text" name="birthday"> <br/>
City: <input type="text" name="city"> <br/>
Language: 
<select name="lang">
	<option value="en">English</option>
	<option value="sp">Spanish</option>
	<option value="cn">Chinese</option>
	<option value="hi">Hindu</option>
	<option value="ar">Arabic</option>
	<option value="tr">Turkish</option>
	<option value="ru">Russian</option>
	<option value="pt">Portuguese</option>
	<option value="gr">Greek</option>
</select>
<br/>
About: <textarea name="about"></textarea> <br/>
<input type="submit" value="Create Account"/>
</form>

<?php 

function checkUsernameExists($username){
	global $db;
	$sql = "SELECT * FROM users 
					WHERE username = '$username'";
	$data = $db->getRowWithSql($sql);
	if(count($data) > 0 && isset($data['user_id']) ){
		echo "Username already exists. Please select another username";
		exit;
	}
}

function checkEmailExists($email){
	global $db;
	$sql = "SELECT * FROM users 
					WHERE email = '$email'";
	$data = $db->getRowWithSql($sql);
	if(count($data) > 0 && isset($data['user_id']) ){
		echo "Email already exists. Please select another email";
		exit;
	}
}

if(!empty($_POST)){
	$username = isset($_POST['username'])? $_POST['username'] : "";
	$password = isset($_POST['password'])? $_POST['password'] : "";
	$full_name = isset($_POST['full_name'])? $_POST['full_name'] : "";
	$email = isset($_POST['email'])? $_POST['email'] : "";
	$gender = isset($_POST['gender'])? $_POST['gender'] : "";
	$birthday = isset($_POST['birthday'])? $_POST['birthday'] : "";
	$city = isset($_POST['city'])? $_POST['city'] : "";
	$lang = isset($_POST['lang'])? $_POST['lang'] : "";
	$about = isset($_POST['about'])? $_POST['about'] : "";

	checkUsernameExists($username);
	checkEmailExists($email);

	if(strlen($username) < 3 || strlen($password) < 3){
		echo "Error on login. Username and Password cannot be empty!";
	}
	else{
		$access_token = md5(uniqid($username, true));
		echo $sql = "INSERT INTO `users` (`username`, `email`, `password`, `full_name`, `gender`, `birthday`, `city`, `lang`, `created_at`, `active_at`, `about`, `access_token`)
		VALUES
			('".$username."', '".$email."', '".$password."', '".$full_name."', '".$gender."', '".$birthday."', '".$city."', '".$lang."', NOW(), NOW(), '".$about."', '".$access_token."')";
		$data = $db->exec($sql);
		$user_id = $db->insert_id;
		if($data){
			echo $location = 'Location: ' . $root_url  .'?page=list_channels&access_token=' . $access_token;
			header($location);
		}
		
	}

	
}

?>